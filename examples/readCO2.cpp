#include <Arduino.h>

#include "rhio-mhv1512a-co2.h"
#include "rhio-pinmap.h"
#define MON RH_UART_DEBUG
//#define DEBUG RH_UART_B
uint8_t results[26];
uint8_t i = 0;
MHV1512A mhv1512a;
void delay_cb(uint32_t millis) {
  //_delay::wait(millis);
  delay(millis);
}

void setup() {
  // put your setup code here, to run once:
  MON.begin(115200);
  RH_UART_B.begin(9600);
  mhv1512a.begin(&RH_UART_B, delay_cb);
  while (!MON) {
  }
  MON.println("INIT MH-V1512A");
}

void loop() {
  // put your main code here, to run repeatedly:
  uint16_t CO2 = mhv1512a.readCO2Concentration();

  MON.print("La concentración de CO2 es: ");
  MON.println(CO2);
  MON.println();
  delay(3000);
}