# Rhio-MH-V1512A-co2

This library allows you to read the CO2 sensor MH-V1512A.

## Main functions

- begin(): Initialize the sensor to listen the Serial provided.
- readCO2Concentration(): This function allows to read the CO2 value and keep the result in a variable.The variable can be accessed to extract concentration of CO2.

There are a series of functions to change settings sensor and calibration. These are the following:

- getCheckSum(): it gets the checkSumcalculation.
- zeroPointCalibration(): it allows to do zero point calibration. (Command calibration).
  There are three methods for zero point calibration.
  - 1: Command calibration using this function.
    Zero Point is 400ppm. Make sure the sensor had been worked under 400ppm for over 20 minutes.
  - 2: Manual calibration.
    Sensor HD pin with low level(0V) and lasting for over 7s(under 400ppm for at least 20 minutes)
  - 3: Automatic Baseline Correction (ABC logic function).
- spanPointCalibration(): it allows to do the span point calibration.
  Do the Zero calibration before span calibration and make sure the sensor worked under a certain level co2 for over 20 minutes. Suggest using 2000ppm as span, at least 1000ppm.

- ABCAutoCalibration(): it allows to enable or disable ABC auto calibration.
  If Byte3 of the request is 0xA0 (ABC on) and if Byte3 is 0x00 (ABC off).
  ABC logic function refers to that sensor itself do zero point judgment and automatic calibration procedure intelligently after a continuous operation period. The automatic calibration cycle is every 24 hours after powered on. The zero point of automatic calibration is 400ppm. This function is usually suitable for indoor air quality monitor such as offices, schools and homes, not suitable for greenhouse, farm and refrigeratory where this function should be off.

- detectionRangeSetting(): it allows to set detection range setting.
  Detection range is 2000 or 5000ppm. Detection range high byte=detection range/256 and detection range low byte=detection range/% 256.

For more information about the sensor see the documentation inside the docs folder.
