/*
    MHV1512A.h - This library allows you to read the MHV1512A CO2 Sensor.
   Created by Raquel Picazo January 12, 2022.
*/
#include "rhio-mhv1512a-co2.h"

#include <Arduino.h>

#include "rhio-pinmap.h"
#define MON RH_UART_DEBUG

MHV1512A::MHV1512A() { _s = NULL; }

void MHV1512A::begin(Stream *ser, void (*delay_cb)(uint32_t param)) {
  _s = ser;
  _delay_cb = delay_cb;
}

uint16_t MHV1512A::readCO2Concentration() {
  // Request
  byte petition[] = {0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};
  int i = 0;
  _s->write(petition, sizeof(petition));

  _s->flush();

  _delay_cb(1500);

  // Read
  if (_s->available() > 0) {
    while (_s->available() > 0) {
      measure[i] = _s->read();
      i++;
      delay(50);
    }
    i = 0;
  }
  if (getCheckSum() == measure[8]) {
    CO2 = measure[2] * 256 + measure[3];
  }
  return CO2;
}

void MHV1512A::manualZeroPointCalibration(uint8_t GPIO) {
  pinMode(GPIO, INPUT);
  digitalWrite(GPIO, LOW);
  delay(7000);
  digitalWrite(GPIO, HIGH);
}

uint8_t MHV1512A::getCheckSum() {
  uint8_t checkSum = 0;
  for (int i = 1; i < 8; i++) {
    checkSum += measure[i];
  }
  checkSum = 255 - checkSum;
  checkSum += 1;

  return checkSum;
}

void MHV1512A::zeroPointCalibration() {
  byte petition[] = {0xFF, 0x01, 0x87, 0x00, 0x00, 0x00, 0x00, 0x78};
  _s->write(petition, sizeof(petition));
  _s->flush();

  _delay_cb(1500);
}
void MHV1512A::spanPointCalibration(int span) {
  byte HIGHBYTE = span / 256;
  byte LOWBYTE = span % 256;
  // Calculate checkSum
  byte petition[] = {0xFF, 0x01, 0x88, HIGHBYTE, LOWBYTE, 0x00, 0x00, 0x00};
  _s->write(petition, sizeof(petition));
  _s->flush();

  _delay_cb(1500);
}
void MHV1512A::ABCAutoCalibration(bool autocalibration) {
  byte ABC;
  if (autocalibration) {
    ABC = 0xA0;
  } else {
    ABC = 0x00;
  }
  // Calculate checkSum
  byte petition[] = {0xFF, 0x01, 0x79, ABC, 0x00, 0x00, 0x00, 0x00};
  _s->write(petition, sizeof(petition));
  _s->flush();

  _delay_cb(1500);
}
void MHV1512A::detectionRangeSetting(byte detectionrange) {
  byte HIGHBYTE = detectionrange * 256;
  byte LOWBYTE = detectionrange % 256;
  // Calculate checkSum
  byte petition[] = {0xFF, 0x01, 0x99, HIGHBYTE, LOWBYTE, 0x00, 0x00, 0x00};
  _s->write(petition, sizeof(petition));
  _s->flush();

  _delay_cb(1500);
}
