/*
    MHV1512A.h - This library allows you to read the MHV1512A CO2 Sensor.
   Created by Raquel Picazo January 12, 2022.
*/
#ifndef MHV1512A_h
#define MHV1512A_h

#include <Arduino.h>
class MHV1512A {
 public:
  MHV1512A();

  void begin(Stream *ser, void (*delay_cb)(uint32_t param));
  /* Public variables */

  /**
   * @fn uint8_t readSensors()
   * @brief This function read the CO2 concentration. It keeps
   * the result of the reading in a buffer called measure which is a private
   * variable of the class MHV1512A. CO2 concentration is kept in a variable
   * called CO2.
   * @return uint16_t
   */
  uint16_t readCO2Concentration();

  /**
   * @fn uint16_t getCheckSum()
   * @brief gets the CheckSumary.
   * @return float
   */
  uint8_t getCheckSum();

  /**
   * @fn uint16_t zeroPointCalibration()
   * @brief this function allows to do zero point calibration.
   * Zero Point is 400ppm. Make sure the sensor had been worked under 400ppm for
   * over 20 minutes.
   * @return (void)
   */
  void zeroPointCalibration();

  /**
   * @fn uint16_t spanPointCalibration()
   * @brief this function allows to do span point calibration.
   *
   * @return (void)
   */
  void spanPointCalibration(int span);

  /**
   * @fn uint16_t ABCAutoCalibration()
   * @brief this function allows to enable or disable ABC auto calibration.
   * @return (void)
   */
  void ABCAutoCalibration(bool autocalibration);

  /**
   * @fn uint16_t detectionRangeSetting()
   * @brief this function allows to set the detection range.
   * @return (void)
   */
  void detectionRangeSetting(byte detectionrange);

  /**
   * @fn uint16_t manualZeroPointCalibration()
   * @brief this function allows to do manual zero point calibration.
   * You have to introduce the GPIO where is conected the pin 1 of the sensor.
   * Zero Point is 400ppm. Make sure the sensor had been worked under 400ppm for
   * over 20 minutes.
   * @return (void)
   */
  void manualZeroPointCalibration(uint8_t GPIO);

 private:
  /* Private variables */
  byte measure[9];
  uint16_t CO2;

  Stream *_s;  // Serial - Serial1 are USARTClass objects.
  void (*_delay_cb)(uint32_t param);
};
#endif
